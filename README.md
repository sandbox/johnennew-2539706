# Mock API

When writing tests for the Drupal functionality, you might not want to
have the Drupal site make actual calls to the remote api. Perhaps there
isn't a testing version or perhaps its not something you have any control
over so cannot remove any test data if it were created there.

This module provides a mock endpoint using the Drupal framework which can
be communicated with during tests instead of the remote API. Functions
exist which allow you to tell the mock API endpoint to expect certain
requests and how to respond to them. A behat context is provided for use
with the drupalextension testing framework. Your behaviour scenarios can
define expected calls to an API and set the expected response.

## Setup

During use, the module stores data into a text file. such as Expected
calls to the webservice. By default this file is _temporary://mock-api.txt_
but this can be set specifically in your settings.php file:

    $conf['mock_api_expectation_file'] = '/tmp/mock-api.txt';

The module provides a single URL which should replace the usual base url
you use to access your real API at your Drupal site URL /mock-api-endpoint.
For example, if your Drupal site under test is http://mysite.local then
the mock API endpoint would be:

    http://mysite.local/mock-api-endpoint
    
You would replace the URL to the real API with this on the test site.

You can then set expected calls to the api and specify the response
to that call.

### Behat using the drupalextension

This module was designed for use with the drupalextension project for behat.

Make sure your configuration is such that it is looking for sub contexts provided
by modules:

https://behat-drupal-extension.readthedocs.org/en/3.0/subcontexts.html#discovering-subcontexts

In short, this means adding the following to the Drupal\DrupalExtension
section of your behat.yml file:

      subcontexts:
              paths:
                - "/path/to/drupal/docroot/sites/all"

## Behat example using the drupalextension

Suppose you had a call which got a user from a CRM system with a REST interface.
Assuming the CRM URL is usually of the form: https://crm.example.com/api/contact/363525
and your Drupal test site URL is http://mysite.local

Enabling the mock_api Drupal module means the Drupal site now has an endpoint of:

    http://mysite.local/mock-api-endpoint
    
In the configuration for your CRM integration, replace https://crm.example.com/api with
http://mysite.local/mock-api-endpoint

During login perhaps Drupal calls that URL to get the users details.
The response is a JSON array with one user object inside it. Inside your
own behat feature context you can then define a function like this to use where
appropriate.

    public function expectRequestUser($id, $name, $email) {

      // Expect the following request.
      $request = (object) array(
        'args' => array('api', 'contact', $id),
        'type' => 'GET',
      );

      $contact = (object) array(
        'id' => $id,
        'name' => $name,
        'email' => $email,
      );

      $result = new stdClass();
      $result->contacts = array($contact);

      // Respond with the following response.
      $response = (object) array(
        'headers' => array(
          'Content-Type' => 'application/json',
        ),
        'status' => 200,
        'body' => json_encode($result),
      );

      // Register this expectation.
      mock_api_set_test_api_response($request, $response);
    }

Whenever, expectRequestUser() function is called, behat will
expect its mock endpoint to be contacted with those details during
the scenario.

Since you will want to fail the test if that request was not made, or if
additional unexpected requests were made to the API, you need to
include the following step later in the scenario after the call to the API
should have taken place.

    Then all expected requests were received by the API

This can be done programatically as well.

    class FeatureContext extends DrupalContext implements SnippetAcceptingContext {
    
      /** @var MockAPIContext */
      private $mockApiContext;
      
      /** @BeforeScenario */
      public function gatherContexts(BeforeScenarioScope $scope) {
        $environment = $scope->getEnvironment();
        $this->mockApiContext = $environment->getContext('MockAPIContext');
      }
      
      /** The expectation function described above. */
      public function expectRequestUser($id, $name, $email) { ... }
    
      /**
       * ...
       */
      public function myCustomStepCode() {
         // Register an expectation that a call to CRM will happen.
         $this->expectRequestUser($id, $name, $email);
         
         /** Some code which would trigger a call to CRM */
         
         // Now check the call to CRM did actually happen.
         $this->mockApiContext->allExpectedRequestsWereReceivedByTheAPI();
      }
    }